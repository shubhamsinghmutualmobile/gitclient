package com.mutualmobile.domain.repositories

import com.mutualmobile.domain.models.GetAccessToken
import com.mutualmobile.domain.utils.SafeResult
import com.mutualmobile.domain.models.GetCode
import com.mutualmobile.domain.models.GetUser

interface IGithubAuthRepo {

  suspend fun getUserCode(): SafeResult<GetCode>
  suspend fun getUserAccessToken(deviceCode: String): SafeResult<GetAccessToken>
  suspend fun getUser(authorization: String): SafeResult<GetUser>

}