package com.mutualmobile.domain.repositories

import com.mutualmobile.domain.models.GetUserRepo
import com.mutualmobile.domain.utils.SafeResult

interface IGithubUserRepo {

  suspend fun getUserRepos(authorization: String): SafeResult<List<GetUserRepo>>

}