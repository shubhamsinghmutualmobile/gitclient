package com.mutualmobile.domain.usecases

import com.mutualmobile.domain.models.GetUser
import com.mutualmobile.domain.repositories.IGithubAuthRepo
import com.mutualmobile.domain.utils.SafeResult
import javax.inject.Inject

class GetUserUseCase @Inject constructor(
  private val githubAuthRepo: IGithubAuthRepo
) {
  suspend operator fun invoke(authorization: String): SafeResult<GetUser> {
    return githubAuthRepo.getUser(authorization)
  }
}