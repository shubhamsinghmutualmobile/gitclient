package com.mutualmobile.domain.usecases

import com.mutualmobile.domain.utils.SafeResult
import com.mutualmobile.domain.models.GetCode
import com.mutualmobile.domain.repositories.IGithubAuthRepo
import javax.inject.Inject

class GetUserCodeUseCase @Inject constructor(
  private val githubAuthRepo: IGithubAuthRepo
) {
  suspend operator fun invoke(): SafeResult<GetCode> {
    return githubAuthRepo.getUserCode()
  }
}