package com.mutualmobile.domain.usecases

import com.mutualmobile.domain.models.GetAccessToken
import com.mutualmobile.domain.utils.SafeResult
import com.mutualmobile.domain.models.GetCode
import com.mutualmobile.domain.repositories.IGithubAuthRepo
import javax.inject.Inject

class GetUserAccessTokenUseCase @Inject constructor(
  private val githubAuthRepo: IGithubAuthRepo
) {
  suspend operator fun invoke(deviceCode: String): SafeResult<GetAccessToken> {
    return githubAuthRepo.getUserAccessToken(deviceCode = deviceCode)
  }
}