package com.mutualmobile.domain.usecases

import com.mutualmobile.domain.models.GetUserRepo
import com.mutualmobile.domain.repositories.IGithubUserRepo
import com.mutualmobile.domain.utils.SafeResult
import javax.inject.Inject

class GetUserReposUseCase @Inject constructor(
  private val githubUserRepo: IGithubUserRepo
) {
  suspend operator fun invoke(authorization: String): SafeResult<List<GetUserRepo>> {
    return githubUserRepo.getUserRepos(authorization)
  }
}