package com.mutualmobile.domain.models

data class GetUserRepo(
    val description: String?,
    val htmlUrl: String?,
    val id: Int?,
    val language: String?,
    val name: String?
  )