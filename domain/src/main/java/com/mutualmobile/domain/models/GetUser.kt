package com.mutualmobile.domain.models

data class GetUser(
  val avatarUrl: String?,
  val email: String?,
  val id: Int?,
  val htmlUrl: String?,
  val name: String?,
  val login: String?
)