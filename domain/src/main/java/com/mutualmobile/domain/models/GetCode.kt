package com.mutualmobile.domain.models

data class GetCode(
    val deviceCode: String?,
    val expiresIn: Int?,
    val interval: Int?,
    val userCode: String?,
    val verificationUri: String?
)