package com.mutualmobile.domain.models

data class GetAccessToken(
    val accessToken: String?,
    val error: String?,
    val errorDescription: String?,
    val errorUri: String?,
    val scope: String?,
    val tokenType: String?
)