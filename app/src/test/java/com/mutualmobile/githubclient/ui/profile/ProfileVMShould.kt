package com.mutualmobile.githubclient.ui.profile

import app.cash.turbine.test
import com.google.common.truth.Truth.assertThat
import com.mutualmobile.data.sources.local.IDataStore
import com.mutualmobile.domain.repositories.IGithubAuthRepo
import com.mutualmobile.domain.repositories.IGithubUserRepo
import com.mutualmobile.domain.usecases.GetUserReposUseCase
import com.mutualmobile.domain.usecases.GetUserUseCase
import com.mutualmobile.githubclient.base.BaseTest
import com.mutualmobile.githubclient.utils.FakeGithubAuthRepo
import com.mutualmobile.githubclient.utils.FakeGithubUserRepo
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import javax.inject.Inject
import kotlin.time.ExperimentalTime

@HiltAndroidTest
class ProfileVMShould : BaseTest() {

  private lateinit var profileVM: ProfileVM
  lateinit var getUserUseCase: GetUserUseCase
  @Inject lateinit var fakePreferencesDatastore: IDataStore
  lateinit var getUserReposUseCase: GetUserReposUseCase
  @Inject @FakeGithubAuthRepo lateinit var githubAuthRepo: IGithubAuthRepo
  @Inject @FakeGithubUserRepo lateinit var githubUserRepo: IGithubUserRepo

  @Before
  fun setUp() {
    getUserUseCase = GetUserUseCase(githubAuthRepo)
    getUserReposUseCase = GetUserReposUseCase(githubUserRepo)
    profileVM = ProfileVM(getUserUseCase, fakePreferencesDatastore, getUserReposUseCase)
  }

  @ExperimentalTime
  @Test
  fun `assert that getUser method successfully inserts user inside user flow`() {
    profileVM.apply {
      runBlocking {
        userFlow.test {
          getUser("testAuth")
          awaitItem()
          assertThat((awaitItem() as UserState.Success).data.name.isNullOrBlank()).isFalse()
          cancelAndIgnoreRemainingEvents()
        }
      }
    }
  }

  @ExperimentalTime
  @Test
  fun `assert that getUserRepos method successfully inserts userRepos inside userRepos flow`() {
    profileVM.apply {
      runBlocking {
        userReposFlow.test {
          getUserRepos("testAuth")
          awaitItem()
          assertThat((awaitItem() as UserRepoState.Success).data.isNotEmpty())
          cancelAndIgnoreRemainingEvents()
        }
      }
    }
  }

  @ExperimentalTime
  @Test
  fun `assert that the user is deleted successfully`() {
    profileVM.apply {
      runBlocking {
        fakePreferencesDatastore.writeAccessToken("TestString")
        userAccessToken.test {
          fakePreferencesDatastore.deleteAccessToken()
          awaitItem()
          assertThat(awaitItem()).isEmpty()
          cancelAndIgnoreRemainingEvents()
        }
      }
    }
  }
}