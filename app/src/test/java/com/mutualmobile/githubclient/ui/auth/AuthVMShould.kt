package com.mutualmobile.githubclient.ui.auth

import app.cash.turbine.test
import com.google.common.truth.Truth.assertThat
import com.mutualmobile.data.sources.local.IDataStore
import com.mutualmobile.domain.repositories.IGithubAuthRepo
import com.mutualmobile.domain.usecases.GetUserAccessTokenUseCase
import com.mutualmobile.domain.usecases.GetUserCodeUseCase
import com.mutualmobile.githubclient.base.BaseTest
import com.mutualmobile.githubclient.utils.FakeGithubAuthRepo
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import javax.inject.Inject
import kotlin.time.ExperimentalTime

@HiltAndroidTest
class AuthVMShould : BaseTest() {

  private lateinit var authVM: AuthVM

  lateinit var getUserCodeUseCase: GetUserCodeUseCase
  @Inject @FakeGithubAuthRepo lateinit var githubAuthRepo: IGithubAuthRepo
  lateinit var getUserAccessTokenUseCase: GetUserAccessTokenUseCase
  @Inject lateinit var fakePreferencesDatastore: IDataStore

  @Before
  fun setUp() {
    getUserCodeUseCase = GetUserCodeUseCase(githubAuthRepo)
    getUserAccessTokenUseCase = GetUserAccessTokenUseCase(githubAuthRepo)
    authVM = AuthVM(getUserCodeUseCase, getUserAccessTokenUseCase, fakePreferencesDatastore)
  }

  @ExperimentalTime
  @Test
  fun `assert that getUserCode method successfully inserts user code inside userCodeStateFlow`() {
    authVM.apply {
      runBlocking {
        userCodeStateFlow.test {
          assertThat(awaitItem() is UserCodeState.Empty)
          getUserCode()
          val result = awaitItem()
          assertThat(result is UserCodeState.Success)
          assertThat((result as UserCodeState.Success).data.deviceCode.isNullOrBlank()).isFalse()
          cancelAndIgnoreRemainingEvents()
        }
      }
    }
  }

  @ExperimentalTime
  @Test
  fun `assert that getUserAccessToken method successfully inserts user access token inside userAccessTokenFlow`() {
    authVM.apply {
      runBlocking {
        userAccessTokenFlow.test {
          getUserAccessToken("testDeviceCode")
          awaitItem()
          assertThat((awaitItem() as UserAccessTokenState.Success).data.accessToken.isNullOrBlank()).isFalse()
          cancelAndIgnoreRemainingEvents()
        }
      }
    }
  }
}