package com.mutualmobile.githubclient

import com.mutualmobile.githubclient.tests.GetUserAccessTokenUseCaseShould
import com.mutualmobile.githubclient.tests.GetUserCodeUseCaseShould
import com.mutualmobile.githubclient.tests.GetUserReposUseCaseShould
import com.mutualmobile.githubclient.tests.GetUserUseCaseShould
import com.mutualmobile.githubclient.ui.auth.AuthVMShould
import com.mutualmobile.githubclient.ui.profile.ProfileVMShould
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.Suite.SuiteClasses

/**
 * Simply run this suite to execute all unit tests available
 * */
@RunWith(Suite::class)
@SuiteClasses(
  GetUserAccessTokenUseCaseShould::class,
  GetUserCodeUseCaseShould::class,
  GetUserReposUseCaseShould::class,
  GetUserUseCaseShould::class,
  AuthVMShould::class,
  ProfileVMShould::class
)
class GithubApiTestSuite