package com.mutualmobile.githubclient.injection

import android.content.Context
import com.mutualmobile.data.injection.StorageModule
import com.mutualmobile.data.sources.local.FakePreferencesDatastore
import com.mutualmobile.data.sources.local.IDataStore
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton

@Module
@TestInstallIn(
  replaces = [StorageModule::class],
  components = [SingletonComponent::class]
)
object FakeStorageModule {

  @Provides
  @Singleton
  fun providesPreferencesDatastore(@ApplicationContext context: Context): IDataStore = FakePreferencesDatastore(context)
}