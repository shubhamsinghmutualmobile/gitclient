package com.mutualmobile.githubclient.injection

import com.mutualmobile.data.repositories.fakeRepos.FakeGithubAuthRepo
import com.mutualmobile.data.repositories.fakeRepos.FakeGithubUserRepo
import com.mutualmobile.domain.repositories.IGithubAuthRepo
import com.mutualmobile.domain.repositories.IGithubUserRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object FakeRepositoryModule {

  @Provides
  @Singleton
  @com.mutualmobile.githubclient.utils.FakeGithubAuthRepo
  fun providesGithubAuthRepo(): IGithubAuthRepo = FakeGithubAuthRepo()

  @Provides
  @Singleton
  @com.mutualmobile.githubclient.utils.FakeGithubUserRepo
  fun providesGithubUserRepo(): IGithubUserRepo = FakeGithubUserRepo()
}