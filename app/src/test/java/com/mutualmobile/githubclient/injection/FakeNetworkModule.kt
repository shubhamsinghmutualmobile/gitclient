package com.mutualmobile.githubclient.injection

import com.mutualmobile.data.injection.NetworkModule
import com.mutualmobile.data.sources.remote.GithubAuthAPI
import com.mutualmobile.data.sources.remote.GithubUserAPI
import com.mutualmobile.githubclient.utils.MockWebUrl
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.mockwebserver.MockWebServer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@TestInstallIn(
  components = [SingletonComponent::class],
  replaces = [NetworkModule::class]
)
object FakeNetworkModule {

  @Provides
  @Singleton
  fun providesOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
    .addInterceptor(HttpLoggingInterceptor().setLevel(BODY))
    .build()

  @Provides
  @Singleton
  fun providesMockWebServer(): MockWebServer = MockWebServer().also { it.start() }

  @Provides
  @Singleton
  @MockWebUrl
  fun providesMockWebUrl(mockWebServer: MockWebServer) = mockWebServer.url("/").toString()

  @Provides
  @Singleton
  fun providesRetrofit(
    @MockWebUrl mockWebUrl: String,
    okHttpClient: OkHttpClient
  ): Retrofit = Retrofit.Builder()
    .baseUrl(mockWebUrl)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

  @Provides
  @Singleton
  fun providesGithubAuthAPI(retrofit: Retrofit) = retrofit.create<GithubAuthAPI>()

  @Provides
  @Singleton
  fun providesGithubUserAPI(retrofit: Retrofit) = retrofit.create<GithubUserAPI>()
}