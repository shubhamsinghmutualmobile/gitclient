package com.mutualmobile.githubclient.tests

import com.google.common.truth.Truth.assertThat
import com.mutualmobile.domain.usecases.GetUserAccessTokenUseCase
import com.mutualmobile.domain.utils.SafeResult
import com.mutualmobile.githubclient.base.BaseTest
import com.mutualmobile.githubclient.utils.enqueueResponse
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class GetUserAccessTokenUseCaseShould : BaseTest() {

  @Inject
  lateinit var getUserAccessTokenUseCase: GetUserAccessTokenUseCase

  @Test
  fun `when api returns success - assert that result data contains access token`() {
    runBlocking {
      mockWebServer.enqueueResponse("github_access_token_success_response.json")

      val result = getUserAccessTokenUseCase("testDeviceCode")
      assertThat(mockWebServer.requestCount).isEqualTo(1)
      assertThat(result is SafeResult.Success)
      assertThat((result as SafeResult.Success).data.accessToken.isNullOrBlank()).isFalse()
    }
  }

  @Test
  fun `when api returns failure - assert that result data contains error message`() {
    runBlocking {
      mockWebServer.enqueueResponse("github_access_token_failure_response.json", 400)

      val result = getUserAccessTokenUseCase("testDeviceCode")
      assertThat(mockWebServer.requestCount).isEqualTo(1)
      assertThat(result is SafeResult.Failure)
      assertThat((result as SafeResult.Failure).message.isNotBlank())
    }
  }
}