package com.mutualmobile.githubclient.tests

import com.google.common.truth.Truth.assertThat
import com.mutualmobile.domain.usecases.GetUserUseCase
import com.mutualmobile.domain.utils.SafeResult
import com.mutualmobile.githubclient.base.BaseTest
import com.mutualmobile.githubclient.utils.enqueueResponse
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class GetUserUseCaseShould : BaseTest() {

  @Inject
  lateinit var getUserUseCase: GetUserUseCase

  @Test
  fun `when api returns success - assert that result data contains user object`() {
    runBlocking {
      mockWebServer.enqueueResponse("github_user_success_response.json")

      val result = getUserUseCase("testAuth")
      assertThat(mockWebServer.requestCount).isEqualTo(1)
      assertThat(result is SafeResult.Success)
      assertThat((result as SafeResult.Success).data).isNotNull()
    }
  }

  @Test
  fun `when api returns failure - assert that result contains error message`() {
    runBlocking {
      mockWebServer.enqueueResponse("github_user_success_response.json", 403)

      val result = getUserUseCase("testAuth")
      assertThat(mockWebServer.requestCount).isEqualTo(1)
      assertThat(result is SafeResult.Failure)
      assertThat((result as SafeResult.Failure).message.isNotBlank())
    }
  }
}