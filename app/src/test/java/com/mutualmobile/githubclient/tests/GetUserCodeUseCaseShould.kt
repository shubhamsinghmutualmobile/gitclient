package com.mutualmobile.githubclient.tests

import com.google.common.truth.Truth.assertThat
import com.mutualmobile.domain.usecases.GetUserCodeUseCase
import com.mutualmobile.domain.utils.SafeResult
import com.mutualmobile.githubclient.base.BaseTest
import com.mutualmobile.githubclient.utils.enqueueResponse
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class GetUserCodeUseCaseShould : BaseTest() {

  @Inject
  lateinit var getUserCodeUseCase: GetUserCodeUseCase

  @Test
  fun `when api returns success - assert that result data contains user code`() {
    runBlocking {
      mockWebServer.enqueueResponse("github_device_code_success_response.json")

      val result = getUserCodeUseCase()
      assertThat(mockWebServer.requestCount).isEqualTo(1)
      assertThat(result is SafeResult.Success)
      assertThat((result as SafeResult.Success).data.deviceCode.isNullOrBlank()).isFalse()
    }
  }

  @Test
  fun `when api returns failure - assert that result contains the error message`() {
    runBlocking {
      mockWebServer.enqueueResponse("github_device_code_failure_response.json", 400)

      val result = getUserCodeUseCase()
      assertThat(mockWebServer.requestCount).isEqualTo(1)
      assertThat(result is SafeResult.Failure)
      assertThat((result as SafeResult.Failure).message.isNotBlank())
    }
  }
}