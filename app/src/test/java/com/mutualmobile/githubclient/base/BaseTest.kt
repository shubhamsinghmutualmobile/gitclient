package com.mutualmobile.githubclient.base

import android.os.Build
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import javax.inject.Inject

@HiltAndroidTest
@RunWith(RobolectricTestRunner::class)
@Config(
  application = HiltTestApplication::class,
  sdk = [Build.VERSION_CODES.O_MR1]
)
abstract class BaseTest {

  @get:Rule var hiltRule = HiltAndroidRule(this)

  @Inject
  lateinit var mockWebServer: MockWebServer

  @Before
  fun setup() {
    hiltRule.inject()
  }

  @After
  fun teardown() {
    mockWebServer.shutdown()
  }
}