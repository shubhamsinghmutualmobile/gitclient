package com.mutualmobile.githubclient.tests

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.airbnb.epoxy.EpoxyRecyclerView
import com.google.common.truth.Truth.assertThat
import com.mutualmobile.domain.models.GetUserRepo
import com.mutualmobile.githubclient.R
import com.mutualmobile.githubclient.ui.controllers.RepoItemController
import com.mutualmobile.githubclient.ui.profile.ProfileActivity
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@Suppress("IllegalIdentifier")
@RunWith(AndroidJUnit4::class)
@LargeTest
class ProfileActivityShould {

  private lateinit var scenario: ActivityScenario<ProfileActivity>

  @Before
  fun setup() {
    scenario = ActivityScenario.launch(ProfileActivity::class.java)
  }

  @Test
  fun `assert that all elements are visible on screen`() {
    onView(withId(R.id.profileActivity_browserButton)).check(matches(isDisplayed()))
    onView(withId(R.id.profileActivity_userName)).check(matches(isDisplayed()))
    onView(withId(R.id.profileActivity_userLink)).check(matches(isDisplayed()))
    onView(withId(R.id.profileActivity_userImage)).check(matches(isDisplayed()))
    onView(withId(R.id.profileActivity_epoxyRv)).check(matches(isDisplayed()))
  }

  @Test
  fun `assert that recyclerView loads items correctly`() {
    scenario.onActivity {
      val recyclerView = it.findViewById<EpoxyRecyclerView>(R.id.profileActivity_epoxyRv)
      val controller = RepoItemController()
      recyclerView.setController(controller)
      controller.apply {
        isLoading = true
        listOfRepos = getListOfRepos()
      }

      recyclerView.adapter?.itemCount?.let { itemCount ->
        assertThat(itemCount > 0)
      }
    }
  }

  private fun getListOfRepos(): List<GetUserRepo> {
    return listOf(
      GetUserRepo("Test Description", "www.mutualmobile.com", 1, "Kotlin", "Test Repo"),
      GetUserRepo("Test Description", "www.mutualmobile.com", 2, "Kotlin", "Test Repo"),
      GetUserRepo("Test Description", "www.mutualmobile.com", 3, "Kotlin", "Test Repo"),
      GetUserRepo("Test Description", "www.mutualmobile.com", 4, "Kotlin", "Test Repo"),
      GetUserRepo("Test Description", "www.mutualmobile.com", 5, "Kotlin", "Test Repo")
    )
  }
}