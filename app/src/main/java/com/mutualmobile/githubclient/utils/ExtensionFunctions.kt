package com.mutualmobile.githubclient.utils

import android.content.Context
import android.net.Uri
import android.view.View
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import com.google.android.material.snackbar.Snackbar

fun Context.shortToast(msg: String) {
  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun View.indefiniteSnackBar(
  msg: String,
  method: () -> Unit
) {
  Snackbar.make(this, msg, Snackbar.LENGTH_INDEFINITE).setAction("Ok") { method() }.show()
}

fun View.longSnackBar(msg: String) {
  Snackbar.make(this, msg, Snackbar.LENGTH_LONG).setAction("Ok") {}.show()
}

fun Context.launchCustomTab(url: String?) {
  url?.let { nnUrl ->
    val customTabsIntent = CustomTabsIntent.Builder().build()
    customTabsIntent.launchUrl(this, Uri.parse(nnUrl))
  }
}