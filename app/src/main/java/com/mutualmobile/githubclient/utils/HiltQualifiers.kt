package com.mutualmobile.githubclient.utils

import javax.inject.Qualifier

@Qualifier
annotation class FakeGithubAuthRepo

@Qualifier
annotation class FakeGithubUserRepo

@Qualifier
annotation class MockWebUrl