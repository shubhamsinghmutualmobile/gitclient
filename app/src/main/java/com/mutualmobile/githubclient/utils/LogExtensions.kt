package com.mutualmobile.githubclient.utils

import android.app.Activity
import android.util.Log
import androidx.fragment.app.Fragment
import com.bugfender.sdk.Bugfender
import com.mutualmobile.githubclient.utils.LogLevel.DEBUG
import com.mutualmobile.githubclient.utils.LogLevel.ERROR

fun Activity.sendLog(
  msg: Any,
  level: LogLevel = DEBUG
) {
  when (level) {
    DEBUG -> Bugfender.d(this::class.java.simpleName, msg.toString())
    ERROR -> Bugfender.e(this::class.java.simpleName, msg.toString())
  }
}

fun Fragment.sendLog(
  msg: Any,
  level: LogLevel = DEBUG
) {
  when (level) {
    DEBUG -> Bugfender.d(this::class.java.simpleName, msg.toString())
    ERROR -> Bugfender.e(this::class.java.simpleName, msg.toString())
  }
}

enum class LogLevel {
  DEBUG,
  ERROR
}