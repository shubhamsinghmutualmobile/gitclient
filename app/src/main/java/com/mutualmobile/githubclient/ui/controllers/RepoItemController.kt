package com.mutualmobile.githubclient.ui.controllers

import com.airbnb.epoxy.EpoxyController
import com.mutualmobile.domain.models.GetUserRepo
import com.mutualmobile.githubclient.R
import com.mutualmobile.githubclient.databinding.RepoItemBinding
import com.mutualmobile.githubclient.ui.controllers.common.EmptyEpoxyItem
import com.mutualmobile.githubclient.ui.controllers.common.EpoxyLoadingItem
import com.mutualmobile.githubclient.utils.ViewBindingKotlinModel
import com.mutualmobile.githubclient.utils.launchCustomTab

class RepoItemController : EpoxyController() {

  var isLoading: Boolean = false
    set(value) {
      field = value
      if (field) {
        requestModelBuild()
      }
    }

  var listOfRepos: List<GetUserRepo> = emptyList()
    set(value) {
      field = value
      isLoading = false
      requestModelBuild()
    }

  override fun buildModels() {
    if (isLoading) {
      EpoxyLoadingItem().id("LoadingItem").addTo(this)
      return
    }

    if (listOfRepos.isNullOrEmpty()) {
      EmptyEpoxyItem().id("EmptyItem").addTo(this)
      return
    }

    listOfRepos.forEach { userRepo ->
      RepoItem(userRepo).id(userRepo.id).addTo(this)
    }
  }

  data class RepoItem(
    val repo: GetUserRepo
  ) : ViewBindingKotlinModel<RepoItemBinding>(R.layout.repo_item) {
    override fun RepoItemBinding.bind() {
      repoItemName.text = repo.name
      repoItemDescription.text = repo.description
      repoItemLanguage.text = repo.language
      repoItemParentCard.setOnClickListener {
        repo.htmlUrl?.let { nnUrl ->
          root.context.launchCustomTab(nnUrl)
        }
      }
    }
  }
}