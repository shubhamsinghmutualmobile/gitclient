package com.mutualmobile.githubclient.ui.controllers.common

import com.mutualmobile.githubclient.R
import com.mutualmobile.githubclient.databinding.LoadingItemBinding
import com.mutualmobile.githubclient.utils.ViewBindingKotlinModel

class EpoxyLoadingItem: ViewBindingKotlinModel<LoadingItemBinding>(R.layout.loading_item) {
  override fun LoadingItemBinding.bind() {
    //nothing to do
  }
}