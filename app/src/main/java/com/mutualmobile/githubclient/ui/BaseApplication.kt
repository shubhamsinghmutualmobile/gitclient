package com.mutualmobile.githubclient.ui

import android.app.Application
import com.bugfender.sdk.Bugfender
import com.mutualmobile.githubclient.BuildConfig
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BaseApplication: Application() {

  override fun onCreate() {
    super.onCreate()

    Bugfender.init(this, BuildConfig.bugFender_key, BuildConfig.DEBUG)
    Bugfender.enableCrashReporting()
    Bugfender.enableUIEventLogging(this)
    Bugfender.enableLogcatLogging()
  }

}