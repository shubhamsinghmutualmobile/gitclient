package com.mutualmobile.githubclient.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.viewbinding.library.activity.viewBinding
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.mutualmobile.domain.models.GetUserRepo
import com.mutualmobile.githubclient.R
import com.mutualmobile.githubclient.R.color
import com.mutualmobile.githubclient.R.string
import com.mutualmobile.githubclient.databinding.ActivityProfileBinding
import com.mutualmobile.githubclient.ui.auth.AuthActivity
import com.mutualmobile.githubclient.ui.controllers.RepoItemController
import com.mutualmobile.githubclient.ui.profile.UserRepoState.Empty
import com.mutualmobile.githubclient.ui.profile.UserRepoState.Failure
import com.mutualmobile.githubclient.ui.profile.UserRepoState.Loading
import com.mutualmobile.githubclient.ui.profile.UserRepoState.NetworkError
import com.mutualmobile.githubclient.ui.profile.UserRepoState.Success
import com.mutualmobile.githubclient.utils.launchCustomTab
import com.mutualmobile.githubclient.utils.longSnackBar
import com.mutualmobile.githubclient.utils.shortToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ProfileActivity : AppCompatActivity() {

  private val binding by viewBinding<ActivityProfileBinding>()
  private val viewModel by viewModels<ProfileVM>()
  private val repoItemController = RepoItemController()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setTheme(R.style.Theme_GithubClient)
    setContentView(R.layout.activity_profile)
    supportActionBar?.apply {
      title = getString(string.profileActivity_actionBarTitle)
      setBackgroundDrawable(ResourcesCompat.getDrawable(resources, color.github_black, theme))
    }
    setupListeners()
  }

  private fun setupListeners() {
    lifecycleScope.launchWhenResumed {
      observeUser()
    }
    lifecycleScope.launchWhenResumed {
      observeUserRepositories()
    }
  }

  private suspend fun observeUser() {
    viewModel.userFlow.collect { userState ->
      when (userState) {
        is UserState.Empty -> Unit
        is UserState.Loading -> manageProgress(true)
        is UserState.Success -> {
          manageProgress(false)
          binding.setupUserData(userState)
        }
        is UserState.Failure -> {
          manageProgress(false)
          binding.root.longSnackBar(userState.message)
        }
        is UserState.NetworkError -> {
          manageProgress(false)
          binding.root.longSnackBar(getString(string.unexpected_network_error))
        }
      }
    }
  }

  private fun ActivityProfileBinding.setupUserData(userState: UserState.Success) {
    userState.data.apply {
      profileActivityUserName.text = name
      profileActivityUserLink.text = htmlUrl
      Glide.with(this@ProfileActivity)
        .load(avatarUrl)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(profileActivityUserImage)
      profileActivityBrowserButton.setOnClickListener {
        this@ProfileActivity.launchCustomTab(htmlUrl)
      }
    }
  }

  private suspend fun observeUserRepositories() {
    viewModel.userReposFlow.collect { userRepoState ->
      when (userRepoState) {
        is Empty -> Unit
        is Loading -> setupEpoxyRecyclerView(null)
        is Success -> setupEpoxyRecyclerView(userRepoState.data)
        is Failure -> {
          binding.root.longSnackBar(userRepoState.message)
          setupEpoxyRecyclerView(null, true)
        }
        is NetworkError -> {
          binding.root.longSnackBar(getString(string.unexpected_network_error))
          setupEpoxyRecyclerView(null, true)
        }
      }
    }
  }

  private fun setupEpoxyRecyclerView(
    listOfRepos: List<GetUserRepo>?,
    removeAdapter: Boolean = false
  ) {
    binding.profileActivityEpoxyRv.setController(repoItemController)
    binding.profileActivityEpoxyRv.adapter = if (removeAdapter) null else repoItemController.adapter
    repoItemController.isLoading = true
    listOfRepos?.let { nnListOfRepos ->
      repoItemController.listOfRepos = nnListOfRepos
    }
  }

  private fun manageProgress(showProgress: Boolean) {
    binding.apply {
      profileActivityProgressBar.isVisible = showProgress
    }
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.profile_menu, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.signout -> {
        manageProgress(true)
        lifecycleScope.launchWhenResumed {
          viewModel.deleteUser()
          viewModel.userAccessToken.collect {
            if (it.isBlank()) {
              val intent = Intent(this@ProfileActivity, AuthActivity::class.java)
              intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
              shortToast(getString(string.signout_successful))
              startActivity(intent)
            }
          }
        }
      }
    }
    return super.onOptionsItemSelected(item)
  }
}