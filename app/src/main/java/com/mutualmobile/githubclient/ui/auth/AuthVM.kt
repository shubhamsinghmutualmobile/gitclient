package com.mutualmobile.githubclient.ui.auth

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mutualmobile.data.sources.local.IDataStore
import com.mutualmobile.domain.models.GetAccessToken
import com.mutualmobile.domain.models.GetCode
import com.mutualmobile.domain.usecases.GetUserAccessTokenUseCase
import com.mutualmobile.domain.usecases.GetUserCodeUseCase
import com.mutualmobile.domain.utils.SafeResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthVM @Inject constructor(
  private val getUserCodeUseCase: GetUserCodeUseCase,
  private val getUserAccessTokenUseCase: GetUserAccessTokenUseCase,
  preferencesDatastore: IDataStore
) : ViewModel() {

  private val _userCodeStateFlow = MutableStateFlow<UserCodeState>(UserCodeState.Empty)
  val userCodeStateFlow = _userCodeStateFlow.asStateFlow()

  private val _userAccessTokenFlow = MutableStateFlow<UserAccessTokenState>(UserAccessTokenState.Empty)
  val userAccessTokenFlow = _userAccessTokenFlow.asStateFlow()

  val userAccessToken = preferencesDatastore.readAccessToken

  fun getUserCode() {
    viewModelScope.launch {
      _userCodeStateFlow.emit(UserCodeState.Loading)
      when (val result = getUserCodeUseCase()) {
        is SafeResult.Success -> _userCodeStateFlow.emit(UserCodeState.Success(result.data))
        is SafeResult.Failure -> _userCodeStateFlow.emit(UserCodeState.Failure(result.message))
        is SafeResult.NetworkError -> _userCodeStateFlow.emit(UserCodeState.NetworkError)
      }
    }
  }

  fun getUserAccessToken(deviceCode: String) {
    viewModelScope.launch {
      _userAccessTokenFlow.emit(UserAccessTokenState.Loading)
      when (val result = getUserAccessTokenUseCase(deviceCode)) {
        is SafeResult.Success -> _userAccessTokenFlow.emit(UserAccessTokenState.Success(result.data))
        is SafeResult.Failure -> _userAccessTokenFlow.emit(UserAccessTokenState.Failure(result.message))
        is SafeResult.NetworkError -> _userAccessTokenFlow.emit(UserAccessTokenState.NetworkError)
      }
    }
  }
}

sealed class UserCodeState {
  object Empty : UserCodeState()
  object Loading : UserCodeState()
  class Success(val data: GetCode) : UserCodeState()
  class Failure(val message: String) : UserCodeState()
  object NetworkError : UserCodeState()
}

sealed class UserAccessTokenState {
  object Empty : UserAccessTokenState()
  object Loading : UserAccessTokenState()
  class Success(val data: GetAccessToken) : UserAccessTokenState()
  class Failure(val message: String) : UserAccessTokenState()
  object NetworkError : UserAccessTokenState()
}