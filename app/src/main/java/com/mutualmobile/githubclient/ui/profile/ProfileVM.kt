package com.mutualmobile.githubclient.ui.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mutualmobile.data.sources.local.IDataStore
import com.mutualmobile.domain.models.GetUser
import com.mutualmobile.domain.models.GetUserRepo
import com.mutualmobile.domain.usecases.GetUserReposUseCase
import com.mutualmobile.domain.usecases.GetUserUseCase
import com.mutualmobile.domain.utils.SafeResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileVM @Inject constructor(
  private val getUserUseCase: GetUserUseCase,
  private val preferencesDatastore: IDataStore,
  private val getUserReposUseCase: GetUserReposUseCase
) : ViewModel() {

  private val _userFlow = MutableStateFlow<UserState>(UserState.Empty)
  val userFlow = _userFlow.asStateFlow()

  private val _userReposFlow = MutableStateFlow<UserRepoState>(UserRepoState.Empty)
  val userReposFlow = _userReposFlow.asStateFlow()

  val userAccessToken = preferencesDatastore.readAccessToken

  init {
    whenAccessTokenIsAvailable()
  }

  private fun whenAccessTokenIsAvailable() {
    viewModelScope.launch {
      userAccessToken.collect {
        if (it.isNotBlank()) {
          getUser(it)
          getUserRepos(it)
        }
      }
    }
  }

  fun getUser(authorization: String) {
    viewModelScope.launch {

      _userFlow.emit(UserState.Loading)

      when (val result = getUserUseCase(authorization)) {
        is SafeResult.Success -> _userFlow.emit(UserState.Success(result.data))
        is SafeResult.Failure -> _userFlow.emit(UserState.Failure(result.message))
        is SafeResult.NetworkError -> _userFlow.emit(UserState.NetworkError)
      }
    }
  }

  fun deleteUser() {
    viewModelScope.launch {
      preferencesDatastore.deleteAccessToken()
    }
  }

  fun getUserRepos(authorization: String) {
    viewModelScope.launch {

      _userReposFlow.emit(UserRepoState.Loading)

      when (val result = getUserReposUseCase(authorization)) {
        is SafeResult.Success -> _userReposFlow.emit(UserRepoState.Success(result.data))
        is SafeResult.Failure -> _userReposFlow.emit(UserRepoState.Failure(result.message))
        is SafeResult.NetworkError -> _userReposFlow.emit(UserRepoState.NetworkError)
      }
    }
  }
}

sealed class UserState {
  object Empty : UserState()
  object Loading : UserState()
  class Success(val data: GetUser) : UserState()
  class Failure(val message: String) : UserState()
  object NetworkError : UserState()
}

sealed class UserRepoState {
  object Empty : UserRepoState()
  object Loading : UserRepoState()
  class Success(val data: List<GetUserRepo>) : UserRepoState()
  class Failure(val message: String) : UserRepoState()
  object NetworkError : UserRepoState()
}