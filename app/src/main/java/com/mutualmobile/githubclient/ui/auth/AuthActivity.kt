package com.mutualmobile.githubclient.ui.auth

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.viewbinding.library.activity.viewBinding
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.mutualmobile.githubclient.R.color
import com.mutualmobile.githubclient.R.layout
import com.mutualmobile.githubclient.R.string
import com.mutualmobile.githubclient.R.style
import com.mutualmobile.githubclient.databinding.ActivityAuthBinding
import com.mutualmobile.githubclient.ui.auth.UserAccessTokenState.Empty
import com.mutualmobile.githubclient.ui.auth.UserAccessTokenState.Failure
import com.mutualmobile.githubclient.ui.auth.UserAccessTokenState.Loading
import com.mutualmobile.githubclient.ui.auth.UserAccessTokenState.NetworkError
import com.mutualmobile.githubclient.ui.auth.UserAccessTokenState.Success
import com.mutualmobile.githubclient.ui.profile.ProfileActivity
import com.mutualmobile.githubclient.utils.AppConstants
import com.mutualmobile.githubclient.utils.indefiniteSnackBar
import com.mutualmobile.githubclient.utils.launchCustomTab
import com.mutualmobile.githubclient.utils.longSnackBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class AuthActivity : AppCompatActivity() {

  private val binding by viewBinding<ActivityAuthBinding>()
  private val viewModel by viewModels<AuthVM>()
  private var wasCustomTabOpened = false
  private lateinit var clipboard: ClipboardManager
  private var clipData: ClipData? = null
  private var deviceCode: String? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setTheme(style.Theme_GithubClient)
    setContentView(layout.activity_auth)
    AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)
    supportActionBar?.apply {
      title = getString(string.welcome_message)
      setBackgroundDrawable(ResourcesCompat.getDrawable(resources, color.github_black, theme))
    }
    clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

    goToProfileIfAccessTokenExists()
    setupListeners()
  }

  private fun goToProfileIfAccessTokenExists() {
    lifecycleScope.launchWhenResumed {
      viewModel.userAccessToken.collect {
        if (it.isNotBlank()) {
          manageProgress(true)
          val intent = Intent(this@AuthActivity, ProfileActivity::class.java)
          intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
          startActivity(intent)
        }
      }
    }
  }

  override fun onResume() {
    super.onResume()
    pollGithubForAccessToken()
  }

  private fun pollGithubForAccessToken() {
    if (wasCustomTabOpened) {
      deviceCode?.let { nnDeviceCode ->
        lifecycleScope.launch {
          viewModel.getUserAccessToken(nnDeviceCode)
          viewModel.userAccessTokenFlow.collect { userAccessTokenState ->
            when (userAccessTokenState) {
              is Empty -> Unit
              is Loading -> {
                manageProgress(true)
              }
              is Success -> {
                wasCustomTabOpened = false
                manageProgress(false)
                userAccessTokenState.data.accessToken?.let { nnUserAccessToken ->
                  binding.root.indefiniteSnackBar("Your access token is: $nnUserAccessToken") {}
                }
              }
              is Failure -> {
                manageProgress(false)
                binding.root.longSnackBar(userAccessTokenState.message)
              }
              is NetworkError -> {
                manageProgress(false)
                binding.root.longSnackBar("Network failure! Please try again later.")
              }
            }
          }
        }
      }
    }
  }

  private fun setupListeners() {
    binding.authActivitySignInButton.setOnClickListener { viewModel.getUserCode() }
    observeUserCodeStateFlow()
  }

  private fun observeUserCodeStateFlow() {
    lifecycleScope.launchWhenResumed {
      viewModel.userCodeStateFlow.collect {
        when (it) {
          is UserCodeState.Empty -> Unit
          is UserCodeState.Loading -> {
            manageProgress(true)
          }
          is UserCodeState.Success -> {
            manageProgress(false)
            copyAndDisplayCodeToUserBeforeLaunchingTab(it)
          }
          is UserCodeState.Failure -> {
            manageProgress(false)
            binding.root.longSnackBar(it.message)
          }
          is UserCodeState.NetworkError -> {
            manageProgress(false)
            binding.root.longSnackBar("Network failure! Please try again later.")
          }
        }
      }
    }
  }

  private fun copyAndDisplayCodeToUserBeforeLaunchingTab(successCodeState: UserCodeState.Success) {
    deviceCode = successCodeState.data.deviceCode
    successCodeState.data.userCode?.let { userCode ->
      if (!wasCustomTabOpened) {
        clipData = ClipData.newPlainText("userCode", userCode)
        clipData?.let { nnClipData ->
          clipboard.setPrimaryClip(nnClipData)
          binding.root.indefiniteSnackBar("Enter '$userCode' when asked (copied to clipboard)") {
            wasCustomTabOpened = true
            this@AuthActivity.launchCustomTab(AppConstants.LOGIN_URL)
          }
        }
      }
    }
  }

  private fun manageProgress(showProgress: Boolean) {
    binding.apply {
      authActivityProgressBar.isVisible = showProgress
      authActivitySignInButton.isEnabled = !authActivityProgressBar.isVisible
    }
  }
}