package com.mutualmobile.githubclient.ui.controllers.common

import com.mutualmobile.githubclient.R
import com.mutualmobile.githubclient.databinding.EmptyLayoutItemBinding
import com.mutualmobile.githubclient.utils.ViewBindingKotlinModel

class EmptyEpoxyItem: ViewBindingKotlinModel<EmptyLayoutItemBinding>(R.layout.empty_layout_item) {
  override fun EmptyLayoutItemBinding.bind() {
    // nothing to do
  }
}