package com.mutualmobile.data.repositories

import com.mutualmobile.data.mappers.toGetUserRepoItem
import com.mutualmobile.data.sources.remote.GithubUserAPI
import com.mutualmobile.data.utils.safeApiCall
import com.mutualmobile.domain.models.GetUserRepo
import com.mutualmobile.domain.repositories.IGithubUserRepo
import com.mutualmobile.domain.utils.SafeResult
import javax.inject.Inject

class GithubUserRepo @Inject constructor(
  private val githubUserAPI: GithubUserAPI
): IGithubUserRepo {
  override suspend fun getUserRepos(authorization: String): SafeResult<List<GetUserRepo>> {
    return when (val result = safeApiCall { githubUserAPI.getUserRepos("token $authorization") }) {
      is SafeResult.Success -> {
        SafeResult.Success(result.data.map { it.toGetUserRepoItem() })
      }
      is SafeResult.Failure -> result
      is SafeResult.NetworkError -> result
    }
  }
}