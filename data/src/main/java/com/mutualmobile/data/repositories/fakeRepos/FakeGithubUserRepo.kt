package com.mutualmobile.data.repositories.fakeRepos

import com.mutualmobile.domain.models.GetUserRepo
import com.mutualmobile.domain.repositories.IGithubUserRepo
import com.mutualmobile.domain.utils.SafeResult

class FakeGithubUserRepo : IGithubUserRepo {
  override suspend fun getUserRepos(authorization: String): SafeResult<List<GetUserRepo>> {
    return SafeResult.Success(listOf(GetUserRepo("test", "test", 1, "test", "test")))
  }
}