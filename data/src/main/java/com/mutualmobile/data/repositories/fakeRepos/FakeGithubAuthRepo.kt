package com.mutualmobile.data.repositories.fakeRepos

import com.mutualmobile.domain.models.GetAccessToken
import com.mutualmobile.domain.models.GetCode
import com.mutualmobile.domain.models.GetUser
import com.mutualmobile.domain.repositories.IGithubAuthRepo
import com.mutualmobile.domain.utils.SafeResult

class FakeGithubAuthRepo : IGithubAuthRepo {
  override suspend fun getUserCode(): SafeResult<GetCode> {
    return SafeResult.Success(GetCode("test", 1, 1, "test", "test"))
  }

  override suspend fun getUserAccessToken(deviceCode: String): SafeResult<GetAccessToken> {
    return SafeResult.Success(GetAccessToken("test", null, null, null, "test", "test"))
  }

  override suspend fun getUser(authorization: String): SafeResult<GetUser> {
    return SafeResult.Success(GetUser("test", "test", 1, "test", "test", "test"))
  }
}