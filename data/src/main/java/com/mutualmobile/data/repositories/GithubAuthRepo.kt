package com.mutualmobile.data.repositories

import com.mutualmobile.data.mappers.toGetAccessToken
import com.mutualmobile.data.mappers.toGetCode
import com.mutualmobile.data.mappers.toGetUser
import com.mutualmobile.data.sources.local.IDataStore
import com.mutualmobile.data.sources.remote.GithubAuthAPI
import com.mutualmobile.data.sources.remote.GithubUserAPI
import com.mutualmobile.data.utils.safeApiCall
import com.mutualmobile.domain.models.GetAccessToken
import com.mutualmobile.domain.models.GetCode
import com.mutualmobile.domain.models.GetUser
import com.mutualmobile.domain.repositories.IGithubAuthRepo
import com.mutualmobile.domain.utils.SafeResult
import javax.inject.Inject

class GithubAuthRepo @Inject constructor(
  private val githubAuthAPI: GithubAuthAPI,
  private val preferencesDatastore: IDataStore,
  private val githubUserAPI: GithubUserAPI
) : IGithubAuthRepo {
  override suspend fun getUserCode(): SafeResult<GetCode> {
    return when (val result = safeApiCall { githubAuthAPI.getUserCode() }) {
      is SafeResult.Success -> SafeResult.Success(result.data.toGetCode())
      is SafeResult.Failure -> result
      is SafeResult.NetworkError -> result
    }
  }

  override suspend fun getUserAccessToken(deviceCode: String): SafeResult<GetAccessToken> {
    return when (val result = safeApiCall { githubAuthAPI.getAccessToken(deviceCode = deviceCode) }) {
      is SafeResult.Success -> {
        val convertedResult = result.data.toGetAccessToken()
        preferencesDatastore.writeAccessToken(convertedResult.accessToken)
        SafeResult.Success(convertedResult)
      }
      is SafeResult.Failure -> result
      is SafeResult.NetworkError -> result
    }
  }

  override suspend fun getUser(authorization: String): SafeResult<GetUser> {
    return when (val result = safeApiCall { githubUserAPI.getUser("token $authorization") }) {
      is SafeResult.Success -> SafeResult.Success(result.data.toGetUser())
      is SafeResult.Failure -> result
      is SafeResult.NetworkError -> result
    }
  }
}