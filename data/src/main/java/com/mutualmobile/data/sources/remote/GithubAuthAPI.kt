package com.mutualmobile.data.sources.remote

import com.mutualmobile.data.BuildConfig
import com.mutualmobile.data.sources.remote.models.GetAccessTokenDto
import com.mutualmobile.data.sources.remote.models.GetCodeDto
import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.Query

interface GithubAuthAPI {

  @POST("device/code")
  suspend fun getUserCode(
    @Query("client_id") clientId: String = BuildConfig.client_id,
    @Query("scope") scope: String = "repo, user"
  ): Response<GetCodeDto>

  @POST("oauth/access_token")
  suspend fun getAccessToken(
    @Query("client_id") clientId: String = BuildConfig.client_id,
    @Query("device_code") deviceCode: String,
    @Query("grant_type") grantType: String = BuildConfig.grant_type,
  ): Response<GetAccessTokenDto>
}