package com.mutualmobile.data.sources.local

import kotlinx.coroutines.flow.Flow

interface IDataStore {
  suspend fun writeAccessToken(accessToken: String?)
  suspend fun deleteAccessToken()
  val readAccessToken: Flow<String>
}