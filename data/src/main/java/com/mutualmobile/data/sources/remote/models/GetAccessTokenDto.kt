package com.mutualmobile.data.sources.remote.models


import com.google.gson.annotations.SerializedName

data class GetAccessTokenDto(
    @SerializedName("access_token")
    val accessToken: String?,
    @SerializedName("error")
    val error: String?,
    @SerializedName("error_description")
    val errorDescription: String?,
    @SerializedName("error_uri")
    val errorUri: String?,
    @SerializedName("scope")
    val scope: String?,
    @SerializedName("token_type")
    val tokenType: String?
)