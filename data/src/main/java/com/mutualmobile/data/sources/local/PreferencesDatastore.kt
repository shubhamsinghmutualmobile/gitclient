package com.mutualmobile.data.sources.local

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map

class PreferencesDatastore constructor(
  private val context: Context
) : IDataStore {

  companion object {
    object PreferenceKey {
      val accessToken = stringPreferencesKey("accessToken")
    }

    val Context.dataStore by preferencesDataStore(
      name = "datastore"
    )
  }

  override suspend fun writeAccessToken(accessToken: String?) {
    if (!accessToken.isNullOrBlank()) {
      context.dataStore.edit { preference ->
        preference[PreferenceKey.accessToken] = accessToken
      }
    }
  }

  override val readAccessToken = context.dataStore.data
    .catch {
      if (this is Exception) {
        emit(emptyPreferences())
      }
    }.map { prefs ->
      prefs[PreferenceKey.accessToken] ?: ""
    }

  override suspend fun deleteAccessToken() {
    context.dataStore.edit { preference ->
      preference.clear()
    }
  }
}