package com.mutualmobile.data.sources.remote

import com.mutualmobile.data.sources.remote.models.GetUserDto
import com.mutualmobile.data.sources.remote.models.userRepos.GetUserRepoDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface GithubUserAPI {

  @GET("user")
  suspend fun getUser(
    @Header("Authorization") authorization: String
  ): Response<GetUserDto>

  @GET("user/repos")
  suspend fun getUserRepos(
    @Header("Authorization") authorization: String
  ): Response<List<GetUserRepoDto>>

}