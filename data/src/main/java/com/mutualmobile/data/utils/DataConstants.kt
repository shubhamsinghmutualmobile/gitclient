package com.mutualmobile.data.utils

object DataConstants {

  const val GITHUB_LOGIN_BASE_URL = "https://github.com/login/"
  const val GITHUB_USER_BASE_URL = "https://api.github.com/"

}