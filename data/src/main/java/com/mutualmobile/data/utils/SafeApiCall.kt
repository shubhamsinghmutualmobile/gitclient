package com.mutualmobile.data.utils

import android.util.Log
import com.mutualmobile.domain.utils.SafeResult
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

private const val TAG = "SafeApiCall"

suspend fun <T> safeApiCall(
  dispatcher: CoroutineDispatcher = Dispatchers.IO,
  method: suspend () -> Response<T>
): SafeResult<T> {
  return withContext(dispatcher) {
    try {
      val result = method()
      if (result.isSuccessful) {
        result.body()?.let {
          Log.d(TAG, "safeApiCall response: $it")
          Log.d(TAG, "safeApiCall: ${result.raw()}")
          SafeResult.Success(it)
        } ?:
        SafeResult.Failure("Unexpected error occurred!")
      } else {
        SafeResult.Failure("Unexpected error occurred!")
      }
    } catch (e: Exception) {
      Log.d(TAG, "safeApiCall error: ${e.localizedMessage}")
      when (e) {
        is IOException -> SafeResult.NetworkError
        is HttpException -> SafeResult.Failure("Unexpected response from server. Please try again!")
        else -> SafeResult.Failure("Unexpected error occurred!")
      }
    }
  }
}