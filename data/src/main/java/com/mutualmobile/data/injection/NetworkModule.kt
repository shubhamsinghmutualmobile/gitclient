package com.mutualmobile.data.injection

import android.content.Context
import com.mutualmobile.data.sources.remote.GithubAuthAPI
import com.mutualmobile.data.sources.remote.GithubUserAPI
import com.mutualmobile.data.utils.DataConstants
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

  @Provides
  @Singleton
  fun providesOkHttpClient(@ApplicationContext context: Context): OkHttpClient = OkHttpClient.Builder()
    .addInterceptor(ChuckInterceptor(context))
    .addNetworkInterceptor {
      val oldRequest = it.request()
      val newRequest = oldRequest.newBuilder().addHeader("Accept", "application/vnd.github.v3+json").build()
      it.proceed(newRequest)
    }.build()

  @Provides
  @Singleton
  @AuthRetrofit
  fun providesAuthRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
    .baseUrl(DataConstants.GITHUB_LOGIN_BASE_URL)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

  @Provides
  @Singleton
  @UserRetrofit
  fun providesUserRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
    .baseUrl(DataConstants.GITHUB_USER_BASE_URL)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

  @Provides
  @Singleton
  fun providesGithubAuthApi(@AuthRetrofit retrofit: Retrofit): GithubAuthAPI = retrofit.create()

  @Provides
  @Singleton
  fun providesGithubUserApi(@UserRetrofit retrofit: Retrofit): GithubUserAPI = retrofit.create()
}