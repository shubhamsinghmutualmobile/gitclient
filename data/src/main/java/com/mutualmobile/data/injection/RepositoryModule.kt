package com.mutualmobile.data.injection

import com.mutualmobile.data.repositories.GithubAuthRepo
import com.mutualmobile.data.repositories.GithubUserRepo
import com.mutualmobile.data.sources.local.IDataStore
import com.mutualmobile.data.sources.remote.GithubAuthAPI
import com.mutualmobile.data.sources.remote.GithubUserAPI
import com.mutualmobile.domain.repositories.IGithubAuthRepo
import com.mutualmobile.domain.repositories.IGithubUserRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

  @Provides
  @Singleton
  fun providesGithubAuthRepo(
    githubAuthAPI: GithubAuthAPI,
    preferencesDatastore: IDataStore,
    githubUserAPI: GithubUserAPI
  ): IGithubAuthRepo = GithubAuthRepo(githubAuthAPI, preferencesDatastore, githubUserAPI)

  @Provides
  @Singleton
  fun providesGithubUserRepo(githubUserAPI: GithubUserAPI): IGithubUserRepo = GithubUserRepo(githubUserAPI)
}