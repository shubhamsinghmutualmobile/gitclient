package com.mutualmobile.data.injection

import android.content.Context
import com.mutualmobile.data.sources.local.IDataStore
import com.mutualmobile.data.sources.local.PreferencesDatastore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object StorageModule {

  @Provides
  @Singleton
  fun providesPreferencesDatastore(@ApplicationContext context: Context): IDataStore = PreferencesDatastore(context)
}