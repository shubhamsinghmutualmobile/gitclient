package com.mutualmobile.data.injection

import javax.inject.Qualifier

@Qualifier
annotation class AuthRetrofit

@Qualifier
annotation class UserRetrofit