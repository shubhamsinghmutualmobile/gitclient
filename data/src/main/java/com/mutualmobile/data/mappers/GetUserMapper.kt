package com.mutualmobile.data.mappers

import com.mutualmobile.data.sources.remote.models.GetUserDto
import com.mutualmobile.domain.models.GetUser

fun GetUserDto.toGetUser() = GetUser(
  avatarUrl = avatarUrl,
  email = email,
  id = id,
  htmlUrl = htmlUrl,
  name = name,
  login = login
)