package com.mutualmobile.data.mappers

import com.mutualmobile.data.sources.remote.models.GetAccessTokenDto
import com.mutualmobile.domain.models.GetAccessToken

fun GetAccessTokenDto.toGetAccessToken() = GetAccessToken(
  accessToken = accessToken,
  error = error,
  errorDescription = errorDescription,
  errorUri = errorUri,
  scope = scope,
  tokenType = tokenType
)