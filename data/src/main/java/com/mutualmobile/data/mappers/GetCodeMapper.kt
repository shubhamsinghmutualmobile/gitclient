package com.mutualmobile.data.mappers

import com.mutualmobile.data.sources.remote.models.GetCodeDto
import com.mutualmobile.domain.models.GetCode

fun GetCodeDto.toGetCode() = GetCode(
  deviceCode = deviceCode,
  expiresIn = expiresIn,
  interval = interval,
  userCode = userCode,
  verificationUri = verificationUri
)