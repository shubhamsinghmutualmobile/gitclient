package com.mutualmobile.data.mappers

import com.mutualmobile.data.sources.remote.models.userRepos.GetUserRepoDto
import com.mutualmobile.domain.models.GetUserRepo

fun GetUserRepoDto.toGetUserRepoItem() = GetUserRepo(
  description = description,
  htmlUrl = htmlUrl,
  id = id,
  language = language,
  name = name
)